package testSmall;
/***
 * 99乘法表简简单单
 * @author 苏慕少
 *
 */

public class 乘法表99 {
	public static void main(String args[]) {
		int i, j,sum = 0;
		for(i = 1;i<=9;i++) {
			for(j=1;j<=i;j++) {
				sum = i*j;
				System.out.print(i+" * "+j+" = "+sum+" ");
			}
			System.out.println();
		}
	}
}
