package testSmall;
/**
 * 测试字符类型和布尔类型
 * @author 苏慕少
 *
 */

public class TestprinmitiveDataType3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char a = 't';
		char b = '尚';
		char c = '\u0061';
		System.out.println(c);
		System.out.println(""+'a'+'\t'+'b');
		System.out.println(""+'a'+'\n'+'b');
		System.out.println(""+'a'+'b');
		String  d = "abc";
		//String就是字符序列
		/**
		 * 和c语言不一样 java只能是true和flase
		 * c语言可以用0或者其他数字代替。
		 */
		
		//测试布尔类型
		boolean man =true;
		if(man)//不推荐man == true
		{
			System.out.println("男性");
		}
		

	}

}
/**
 * less is more 少代替多
 */
