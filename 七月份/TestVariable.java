package testSmall;
/**
 * 测试变量
 * @author 苏慕少
 * 声明且初始化
 */

public class TestVariable {
	int a;//成员变量,会自动被初始化属于对象
	static int size;//从属于类
	public static void main(String[] args) {
		{
			int age;//局部变量属于语句块
			age = 18;
		}
		
		int salary  = 3000;
		int gao = 10;//局部变量属于方法
		System.out.println(gao);
		}
}
/**
 * 成员变量
 * 静态变量 static 周期长
 */
