package testSmall;
/***
 * 三目运算符/三元运算符/条件运算符
 * 1、语法规则
 * 布尔表达式？表达式1：表达式2
 * 2、三元运算符的执行原理？
 * 当布尔表达式的结果是true的时候，选择表达式1作为整个表达式的执行结果
 * @author 苏慕少
 *
 */

public class OperatorTest2 {
	public static void main(String[] args) {
		boolean sex = true;
		char x = sex ? '男':'女';
		System.out.println("值为="+x);
		System.out.println(sex ? '男':'女');
		
	}

}
