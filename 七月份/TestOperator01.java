package testSmall;
/**
 * 测试算数运算符
 * @author 苏慕少
 *
 */

public class TestOperator01 {
	public static void main(String args[]) {
		/**
		byte a=1;
		int b =2;
		a/=b;
		a-=b;
		a+=b;
		a%=b;
		long b2 =3;
		//byte c =a+b;//报错
		//int c2 = b2+b;//报错
		float f1  = 3.14F;
		double d = b + b2;
		System.out.println(-9%5);
		*/
		//测试自增自减
		int a = 3;
		int b = a++;//执行完后b=3，先给b赋值，再自增
		System.out.println("a="+a+"\nb="+b);
		a=3;
		b=++a;//执行完后b=5，，a先自增，再给c赋值
		System.out.println("a="+a+"\nc="+b);
	}
}
