package testSmall;
/**  1、continue表示：继续/ go on /下一个

     2、continue也是一个continue关键字加一个分号构成一个单独完整的java语句，主要出现循环语句当中用来控制循环的执行。

     3、break和continue的区别

            break表示循环不执行了。

            continue表示直接进入下一次循环继续执行。
 * @author 苏慕少
 *
 */

public class Continue测试 {
	public static void main(String[] args) {
		for(int i = 0;i < 10; i++) {
			if(i==5) {
				break;
				}
			System.out.println(" i = " + i);//1234
		}
		System.out.println("hello word!");
	
		for(int i = 0;i < 10; i++) {//12346789
			if(i==5) {
				continue;
			}
				System.out.println("i="+i);
			}
		System.out.println("hello word!");
	}
}

	

