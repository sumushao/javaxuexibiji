package testSmall;
/**
 * 用来测试注释的用法
 * @author 苏慕少
 *
 */
public class myunit2 {
	/**
	 * 这是程序的入口
	 * @param args
	 */
	public  static void main(String[] args/*args参数名*/) {
		System.out.println("测试注释");//这里是打印语句
	}
	/*
	 *我是多行注释
	 **我是多行注释
	 **我是多行注释
	 **我是多行注释
	 **我是多行注释
	 */

}
