package testSmall;
/***
 * 需求：
 * 假设系统给定一个人的年龄（需要从键盘输入），
 * [0-5] 幼儿
 * [6-10] 少年
 * [11-18] 青少年
 * [19-35] 青年
 * [36-55] 中年
 * [56-150] 老年
 * @author 苏慕少
 *
 */

public class 从解盘输入一个字符 {
	public static void main(String args[]) {
		java.util.Scanner s =new java.util.Scanner(System.in);
		System.out.print("请输入你的年龄：");
		int age = s.nextInt();
		String str = "老年";
		if(age<0||age>150) {
			str="您提供的年龄不合法，年龄值需要在[0-150]之间";
		}
		else if(age<=5){
			str= "幼儿";
		}
		else if(age<=10){
			str= "少年";
		}
		else if(age<=18){
			str= "青少年";
		}
		else if(age<=35){
			str= "青年";
		}
		else if(age<=55){
			str= "中年";
		}
		
		System.out.println("您处于生命周期的"+str+ "阶段");
		
	}

}
