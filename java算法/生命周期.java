/**
 * 这是一个生命周秋的编程
 * 
 * @author 苏慕少
 *
 */
import java.util.Scanner;
public class 生命周期 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int p,e,i,d,caseNo=0;
		int N = 21252;
		Scanner input = new Scanner(System.in);
		p = input.nextInt();
		e = input.nextInt();
		i = input.nextInt();
		d = input.nextInt();
		
		while(p!=-1) {
			caseNo ++;
			int  k;
			for(k=p+1;(k-p)%23<N;k++);
			for(;(k-e)%28<N;k+=23);
			for(;(k-i)%33<N;k+=23*28);
			System.out.println("case:"+caseNo+":the next triple peak occurs in "+(k-d));
		}

	}

}
