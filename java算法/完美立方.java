/**
 * 完美立方：a*a*a=b*b*b+c*c*c+d*d*d;
 */
import java.util.Scanner;
public class 完美立方 {
	public static void main(String[] args) {
		int N;
		Scanner S = new Scanner(System.in);
		N = S.nextInt();
		int a,b,c,d;
		for(a=2;a<=N;a++) {
			for(b=2;b<a;b++) {
				for(c=b;c<a;c++) {
					for(d=c;d<a;d++) {
						if(a*a*a==b*b*b+c*c*c+d*d*d) {
							System.out.println("Cube="+a+",Triple=("+b+","+c+","+d+")");
						}
					}
				}
			}
		}
	}
}
