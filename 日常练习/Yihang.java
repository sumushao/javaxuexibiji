package Study;
import java.util.Scanner;
class Yinghang{
	public class BankAccount {
			private String m_iName;
			private double m_dBalance;
			private double m_dInterestRate;
		public BankAccount(){
			m_iName="";
			m_dBalance=0.0;
			m_dInterestRate=0.0;
		}
		public BankAccount(String name, double balance, double interestRate) {
			m_iName = name;
			m_dBalance = balance;
			m_dInterestRate = interestRate;
		}
		public String getM_iName() {
			return m_iName;
		}
		public void setM_iName(String name) {
			m_iName = name;
		}
		public double getBalance() {
			return m_dBalance;
		}
		public void setM_dBalance(double balance) {
			m_dBalance = balance;
		}

		public void printAccountMsg(){
			System.out.println("帐号："+m_iName+" 当前余额"+m_dBalance+"元" );
		}
		public void saveMoney(double money){
			m_dBalance+=money;
		}
		public void getMoney(double money){
		if(money<=m_dBalance)
			m_dBalance-=money;
		else
			System.out.println("至少保留余额0.01元！");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("欢迎使用陶氏银行");
		System.out.println("请输入数字进行操作1、开户2、存款3、查询4、取款5、退出");
	}
}