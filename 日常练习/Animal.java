package text;

public interface Animal{
	void run() ;
}
class Fish implements Animal{
	public void run() {
		System.out.println("鱼在水里游");
	}
}
class Bird implements Animal{
	public void run() {
		System.out.println("鸟在天上飞");
	}
}
class TextAnimal{
	public static void mian(String[] args) {
	Fish fish=new Fish();
	Bird bird=new Bird();
	fish.run();
	bird.run();
}
}