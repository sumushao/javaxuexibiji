package Study;
class OverFlowException extends Exception{
	OverFlowException(){
		System.out.println("此处数据有溢出，溢出类是OverFlowException");
	}
}
public class Exception5 {
	public static int x=100000;
	public static int multi() throws OverFlowException{
		int aim;
		aim =x*x*x;
		if(aim>1.0E8 || aim<0) {
			throw new OverFlowException();
		}
		else
			return x*x;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int y;
		try
		{
			y=multi();
			System.out.println("y="+y);
		}
		catch(OverFlowException e)
		{
			System.out.println(e);
		}

	}

}
