package text;
class Rectangle{
	float width,height;
	public Rectangle(float width,float height) {
		this.width=width;
		this.height=height;
	}
	public float getLength() {
		return(this.height+this.width)*2;
	}
	public float getArea() {
		return (this.height*this.width);
	}
}

public class KaoShi {

	public static void main(String[] args) {
		Rectangle cr1 = new Rectangle(100,200);
		// TODO Auto-generated method stub
		System.out.println("周长是"+cr1.getLength());
	
		System.out.println("面积为"+cr1.getArea());
	}

}
