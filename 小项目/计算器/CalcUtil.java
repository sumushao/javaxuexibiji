package cn.hxzy.calc;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CalcUtil {
	public static Image getImage(String path){
		BufferedImage image=null;
		File file=new File(path);
		try {
			FileInputStream input=new FileInputStream(file);
			image=ImageIO.read(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	} 
}
