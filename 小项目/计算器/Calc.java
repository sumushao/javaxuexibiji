package cn.hxzy.calc;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Calc extends JFrame {
	private static final long serialVersionUID = 1L;
	// 定义输入框
	JTextField result = null;
	// 定义18个按钮
	JButton jb1, jb2, jb3, jb4, jb5, jb6, jb7, jb8, jb9, jb0, jbpointer, jbadd, jbsub, jbmub, jbcom, jbspace, jbdelete,
			jbprive;
	Double a, b;
	String input = "", output = "", calc = "";

	public void calcFrame() {
		this.setTitle("计算器");// 设置标题
		this.setSize(300, 250);// 设置大小
		this.setVisible(true);// 设置可见
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 设置关闭方式
		this.setLayout(null);// 设置布局
		this.setIconImage(CalcUtil.getImage("image/tubiao.png"));// 设置图标
		this.setResizable(false);// 禁止放大
		Componct();
		this.paint(this.getGraphics());
		this.paintComponents(this.getGraphics());
		this.paintAll(this.getGraphics());
	}

	public void Componct() {
		result = new JTextField();
		result.setBounds(20, 20, 260, 30);
		result.setHorizontalAlignment(JTextField.RIGHT);
		this.add(result);

		jb1 = new JButton("1");
		jb1.setBounds(20, 70, 50, 20);
		jb1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 1;
					result.setText(output);
				} else {
					input += 1;
					result.setText(input);
				}
			}
		});
		this.add(jb1);

		jb2 = new JButton("2");
		jb2.setBounds(80, 70, 50, 20);
		jb2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 2;
					result.setText(output);
				} else {
					input += 2;
					result.setText(input);
				}
			}
		});
		this.add(jb2);

		jb3 = new JButton("3");
		jb3.setBounds(140, 70, 50, 20);
		jb3.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 3;
					result.setText(output);
				} else {
					input += 3;
					result.setText(input);
				}
			}
		});
		this.add(jb3);

		jb4 = new JButton("4");
		jb4.setBounds(200, 70, 50, 20);
		jb4.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 4;
					result.setText(output);
				} else {
					input += 4;
					result.setText(input);
				}
			}
		});
		this.add(jb4);

		jb5 = new JButton("5");
		jb5.setBounds(20, 100, 50, 20);
		jb5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 5;
					result.setText(output);
				} else {
					input += 5;
					result.setText(input);
				}
			}
		});
		this.add(jb5);

		jb6 = new JButton("6");
		jb6.setBounds(80, 100, 50, 20);
		jb6.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 6;
					result.setText(output);
				} else {
					input += 6;
					result.setText(input);
				}
			}
		});
		this.add(jb6);

		jb7 = new JButton("7");
		jb7.setBounds(140, 100, 50, 20);
		jb7.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 7;
					result.setText(output);
				} else {
					input += 7;
					result.setText(input);
				}
			}
		});
		this.add(jb7);

		jb8 = new JButton("8");
		jb8.setBounds(200, 100, 50, 20);
		jb8.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 8;
					result.setText(output);
				} else {
					input += 8;
					result.setText(input);
				}
			}
		});
		this.add(jb8);

		jb9 = new JButton("9");
		jb9.setBounds(20, 130, 50, 20);
		jb9.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 9;
					result.setText(output);
				} else {
					input += 9;
					result.setText(input);
				}
			}
		});
		this.add(jb9);

		jb0 = new JButton("0");
		jb0.setBounds(80, 130, 50, 20);
		jb0.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (calc.equals("+") || calc.equals("-") || calc.equals("*") || calc.equals("/")) {
					result.setText("");
					output += 0;
					result.setText(output);
				} else {
					input += 0;
					result.setText(input);
				}
			}
		});
		this.add(jb0);

		jbadd = new JButton("+");
		jbadd.setBounds(140, 130, 50, 20);
		jbadd.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				calc = "+";
				result.setText(input);
			}
		});
		this.add(jbadd);

		jbsub = new JButton("-");
		jbsub.setBounds(200, 130, 50, 20);
		jbsub.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				calc = "-";
				result.setText(input);
			}
		});
		this.add(jbsub);

		jbmub = new JButton("*");
		jbmub.setBounds(20, 160, 50, 20);
		jbmub.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				calc = "*";
				result.setText(input);
			}
		});
		this.add(jbmub);

		jbcom = new JButton("/");
		jbcom.setBounds(80, 160, 50, 20);
		jbcom.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				calc = "/";
				result.setText(input);
			}
		});
		this.add(jbcom);

		jbprive = new JButton("=");
		jbprive.setBounds(140, 160, 50, 20);
		jbprive.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				a = Double.parseDouble(input);
				b = Double.parseDouble(output);
				switch (calc) {
				case "+":
					result.setText((a+b)+"");
					break;
				case "-":
					result.setText((a-b)+"");
					break;
				case "*":
					result.setText((a*b)+"");
					break;
				case "/":
					if(b==0){
						result.setText("NAN");
					}else{
						result.setText((a/b)+"");
					}
					break;
				}
			}
		});
		this.add(jbprive);

		jbpointer = new JButton(".");
		jbpointer.setBounds(200, 160, 50, 20);
		jbpointer.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				input += ".";
				result.setText(input);
			}
		});
		this.add(jbpointer);

		jbspace = new JButton("←");
		jbspace.setBounds(20, 190, 110, 20);
		this.add(jbspace);

		jbdelete = new JButton("C");
		jbdelete.setBounds(140, 190, 110, 20);
		this.add(jbdelete);
	}

	public static void main(String[] args) {
		new Calc().calcFrame();
	}
}
