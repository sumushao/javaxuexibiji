package com.sumushao.xml;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Dom4jText {

	public static void main(String[] args) {
		try {
			//创建解析器
			SAXReader reader = new SAXReader();
			//通过解析器的read方法配置文件读取到内存中，生成一个Document[org.dom4j]对象树
			Document document = reader.read("conf/Students.xml");
			//获取根节点
			Element root = document.getRootElement();
			//开始遍历跟节点
			for(Iterator<Element>rootIter = root.elementIterator();rootIter.hasNext();) {
				Element studenElt = rootIter.next();
				for(Iterator<Element> innerIter = studenElt.elementIterator();innerIter.hasNext();) {
					Element innerElt = innerIter.next();
					String innerValue = innerElt.getStringValue();
					System.out.println(innerValue);
				}
				System.out.println("---------------------");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
