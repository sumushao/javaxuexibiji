package com.sumushao.xml;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class SAXParserTest {

	public static void main(String[] args) {
		
		try {
			//创建解析工厂
			SAXParserFactory saxParserFactory= SAXParserFactory.newInstance();
			//创建解析器
			SAXParser saxParse =saxParserFactory.newSAXParser();
			//通过解析器Parser方法
			saxParse.parse("conf/persons.xml",new MyDefaultHandler());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

}
class MyDefaultHandler extends DefaultHandler{

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) 
			throws SAXException {
		// TODO Auto-generated method stub
		System.out.print("<" + qName + ">");
	}


	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		System.out.println(new String(ch,start,length));
	}



	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// TODO Auto-generated method stub
		System.out.print("</" + qName + ">");
	}
	

	
	
	
}