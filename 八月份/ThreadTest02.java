package EightStudy;
/**
 * 在java语言中实现多线程的方法
 * 继承java.lang.Thread
 * 重写run方法
 * @author 苏慕少
 * 如何定义一个线程
 *如何创建线程？
 *如何启动线程？
 */

public class ThreadTest02 {
	public static void main(String[] args) {
		//创建线程
		Thread t = new Processor();
		//启动
		t.start();
		
		
		//t.run();这是普通方法调用就只有一个栈，先执行t在执行main
		//这段代码在主线程中运行
		for(int i =0;i<100;i++) {
			System.out.println("main->"+i);
		}
		//有了多线程之后，面方法结束只是主线程中没有方法栈帧了。
		//但是其他线程或者其他栈中还有栈帧。
		//main方法结束，程序可能还在运行。
		
	}

}
//定义一个线程
class Processor extends Thread{
	public void run() {
		for(int i =0;i<100;i++) {
			System.out.println("run-->"+i);
		}
	}
	
}
