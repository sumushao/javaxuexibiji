package EightStudy;
/***
 * 同一个包下，可以随便调用类
 * @author 苏慕少
 *带static 类名.方法名();
 *不带static 引用.方法名();
 */

public class TestUser {
	public static void main(String[] args) {
		Usertest o = new Usertest();
		o.P();
		o.setAge(16);
		System.out.println(o.getAge());
		
	}

}
