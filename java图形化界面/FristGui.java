package java图形化界面;
import javax.swing.*;
/**
 * 第一个GUI
 * @author www80
 *JFrame代表屏幕上的windows对象
 *上面我们可以放button text checkbox
 */
public class FristGui {
	public static void main(String[] args) {
		//创建frame和button
		JFrame frame = new JFrame();
		JButton button = new JButton("click me");
		//程序在windows结束时关闭程序
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//把button添加到frame中
		frame.getContentPane().add(button);
		//设置窗口大小
		frame.setSize(300,300);
		//设置窗口可见性
		frame.setVisible(true);
	}
	

}
