package java图形化界面;
import javax.swing.*;
import java.awt.event.*;


public class SetButtonFunction implements ActionListener {
	JButton button;
	
	public static void main(String[] args) {
		SetButtonFunction gui = new SetButtonFunction();
		gui.go();
	}
		//创建frame和button
		public void go() {
			JFrame frame = new JFrame();
			JButton button = new JButton("click me");
			
			button.addActionListener(this);
			
			
			//把button添加到frame中
			frame.getContentPane().add(button);
			//程序在windows结束时关闭程序
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//设置窗口大小
			frame.setSize(300,300);
			//设置窗口可见性
			frame.setVisible(true);
		}
	public void actionPerformed(ActionEvent event) {
		button.setText("I've  been clicked");
	}

}
